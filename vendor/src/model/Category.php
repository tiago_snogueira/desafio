<?php
namespace Project\Model;

use Project\DB\Sql;

Class Category {


    public static function listAll()
	{

		$sql = new Sql();

		$res = $sql->select("SELECT id, descricao FROM categoria ");

        return $res;

	}

    public static function save($post){

        $sql = new Sql();

        if(!empty($post['id'])){
            
            $sql->query("UPDATE categoria SET descricao = :descricao, codigo = :codigo WHERE id = :id ", array(
                ":descricao" => $post['nome'],
                ":codigo" => $post['codigo'],
                ":id" => $post['id']
            ));

        } else {

            $sql->query("INSERT INTO categoria (descricao, codigo) VALUES ( :descricao, :codigo )", array(
                ":descricao" => $post['nome'],
                ":codigo" => $post['codigo']
            ));
        }


    }

    public static function getCategoryById($id){

        $sql = new Sql();
	
        $results = $sql->select("SELECT * FROM categoria WHERE id = :id", [
            ':id'=>$id
        ]);

        return $results[0];

    }

    public function delete($get)
    {

        $sql = new Sql();

        $sql->query("DELETE FROM categoria WHERE id = :id", [
            ':id'=> $get['id']
        ]);

    }

}

?>