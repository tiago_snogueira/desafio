<?php
namespace Project\Model;

use Project\DB\Sql;

Class Products {


    public static function listAll()
	{

		$sql = new Sql();

		return $sql->select("SELECT p.id, p.nome, p.sku, p.preco, p.descricao, p.quantidade, c.descricao as categoria FROM produto AS p JOIN categoria AS c ON p.id_categoria = c.id ");

	}

    public static function save($post){

        $sql = new Sql();

        $price = str_replace(",", ".", str_replace(".", "", $post['price']));

        if(!empty($post['id'])){

            $sql->query("UPDATE produto SET nome = :nome, sku = :sku, preco = :preco, quantidade = :quantidade, descricao = :descricao, id_categoria = :id_categoria WHERE id = :id ", array(
                ":nome" => $post['name'],
                ":sku" => $post['sku'],
                ":preco" => $price,
                ":descricao" => $post['description'],
                ":quantidade" => $post['quantity'],
                ":id_categoria" => $post['categorie'],
                ":id" => $post['id']
            ));

            $id = $post['id'];

        } else {
            
           $id = $sql->query("INSERT INTO produto (nome, sku, preco, descricao, quantidade, id_categoria) VALUES (:nome, :sku, :preco, :descricao, :quantidade, :id_categoria )", array(
                ":nome" => $post['name'],
                ":sku" => $post['sku'],
                ":preco" => $price,
                ":descricao" => $post['description'],
                ":quantidade" => $post['quantity'],
                ":id_categoria" => $post['categorie']
            ));

            // $last_id = $this->conn->lastInsertId();


        }

        return $id;

        

        // exit();

    }

    public static function getProductById($id){

        $sql = new Sql();
	
        $results = $sql->select("SELECT id, nome, sku, preco, descricao, quantidade, id_categoria FROM produto WHERE id = :id", [
            ':id'=>$id
        ]);

        return $results[0];

    }

    
    public function delete($get)
    {

        $sql = new Sql();
        
        $sql->query("DELETE FROM produto_img WHERE id_produto = :id", [
            ':id'=> $get['id']
        ]);

        $sql->query("DELETE FROM produto WHERE id = :id", [
            ':id'=> $get['id']
        ]);

        $path = "../res/img";
        unlink($path . "/" . $get['id'].'.jpg');

    }

    public function setPhoto($file, $id)
	{

        // se existir imagem para o produto, deleta e insere outra
        
        if(!empty($file['name'])){

            $sql = new Sql();
            
            $sql->query("DELETE FROM produto_img WHERE id_produto = :id", [
                ':id'=> $id
            ]);

            $path = "/res/img";
            unlink($path . "/" . $id);
            
            $extension = explode('.', $file['name']);
            $extension = end($extension);

            switch ($extension) {

                case "jpg":
                case "jpeg":
                $image = imagecreatefromjpeg($file["tmp_name"]);
                break;

                case "gif":
                $image = imagecreatefromgif($file["tmp_name"]);
                break;

                case "png":
                $image = imagecreatefrompng($file["tmp_name"]);
                break;

            }

            $dist = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 
                "res" . DIRECTORY_SEPARATOR . 		
                "img" . DIRECTORY_SEPARATOR . 			
                $id . ".jpg";

            imagejpeg($image, $dist);

            imagedestroy($image);

            $sql = new Sql();

            $sql->query("INSERT INTO produto_img (id_produto, nome_foto, nome_temp) VALUES (:id_produto, :nome_foto, :nome_temp )", array(
                    ":id_produto" => $id,
                    ":nome_foto" => $file['name'],
                    ":nome_temp" => $id
                ));

        }
		

	}

    public static function getFileById($id){

        $sql = new Sql();
	
        $results = $sql->select("SELECT id, id_produto, nome_foto, nome_temp FROM produto_img WHERE id_produto = :id", [
            ':id'=>$id
        ]);

        return $results[0];
        
    }

}

?>