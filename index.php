<?php
require_once("vendor/autoload.php");

use \Project\DB\Sql;
use Project\Model\Category;
use Project\Model\Products;

if($_POST['action'] == "saveProduct"){

    $product = new Products();

    $id = $product->save($_POST);

    $product->setPhoto($_FILES['file'], $id);
    
    header('Location: http://www.desafio.com.br/assets/products.php');
    exit;


} else if ($_POST['action'] == "saveCategory"){

    $category = new Category();

    $category->save($_POST);
   
    
    header('Location: http://www.desafio.com.br/assets/categories.php');
    exit;
} else if ($_GET['action'] == "deleteCategory"){

    $category = new Category();

    $category->delete($_GET);
    
    header('Location: http://www.desafio.com.br/assets/categories.php');
    exit;
    
} else if ($_GET['action'] == "deleteProduct"){

    $product = new Products();
    
    $product->delete($_GET);

    header('Location: http://www.desafio.com.br/assets/products.php');
    exit;
}

header('Location: http://www.desafio.com.br/assets/products.php');
exit;

?>